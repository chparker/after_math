"""
This is a Python application that will generate a basic math question to be solved by the user.

The application should be run via a cronjob once every x hours.

"""
import math
import random
from operator import add, sub
from tkinter import *
from tkinter import messagebox

class Math_puzzle:


    def __init__(self):
        self.number_one = random.randrange(100,999)
        self.number_two = random.randrange(100,999)
        self.ops = (add, sub)
        self.op = random.choice(self.ops)
        self.puzzle()


    def puzzle(self):
        if self.op == sub:
            self.oper = "-"
        else:
            self.oper = "+"
        self.correct = self.op(self.number_one, self.number_two)
        self.mathq = ("What's the anwser to " + str(self.number_one) + " " + str(self.oper)+ " " + str(self.number_two))


    def is_correct(self, given_answer):
            if self.correct == given_answer:
                return True
            else:
                return False




class window_manager:


    def __init__(self, new_puzzle):
        self.new_puzzle = new_puzzle
        window = Tk()
        window.title("After Math Puzzles")
        window.geometry("400x100")
        self.app = Frame(window)
        #app.grid()

        bottomframe = Frame(window)
        bottomframe.pack(side = BOTTOM)

        q_label = Label(self.app, text = new_puzzle.mathq, width=len(new_puzzle.mathq))
        q_label.pack(side = TOP)

        entrylabel1 = Label(self.app)
        entrylabel1["text"] = "Your answer: "
        entrylabel1.pack(side=LEFT)

        self.entry1 = Entry(self.app)
        #entry1.grid(row=2, column=1)
        self.entry1.pack(side = RIGHT)
        self.entry1.focus_set()

        #sub = lambda: self.submit()

        #entry Button
        btn_submit = Button(bottomframe, text = "Submit", command = self.submit)
        btn_submit.pack(side = BOTTOM)

        self.app.pack()


        window.mainloop()


    def submit(self):
        if self.entry1.get().strip() == "":
            messagebox.showerror("Please enter a number.")
            return

        try:
            answer = int(self.entry1.get().strip())
        except ValueError:
            messagebox.showerror("Please enter a number.")
            return

        if self.new_puzzle.is_correct(answer):
              messagebox.showinfo("Answer", "That's Correct!")
              self.app.quit()
        else:
            messagebox.showinfo("Answer", "That's Incorrect!")


def main():
    new_puzzle = Math_puzzle()
    wm = window_manager(new_puzzle)

if __name__ == "__main__":
    main()
